package utils

import (
	"encoding/json"
	"getir/types"
	"io"
)

// Decode - decodes the request body and extends the validator interface
func Decode(body io.ReadCloser, v interface{}) error {
	if err := json.NewDecoder(body).Decode(v); err != nil {
		return err
	}

	if payload, ok := v.(types.Ok); ok {
		return payload.Ok()
	}
	return nil
}
