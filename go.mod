module getir

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/go-chi/chi/v5 v5.0.7
	github.com/onsi/ginkgo v1.16.5
	github.com/onsi/gomega v1.17.0
	go.mongodb.org/mongo-driver v1.8.1
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
)
