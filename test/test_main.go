package test

import (
	"getir/config"
	"getir/memory"
	"getir/store"
	"log"
)

// MainSetup common test setup, db, redis, env, and migrations
func MainSetup() {
	// load all config/env's
	config.Init()
	// override the test env's
	Setup()
	// init db connection
	store.Init()
	// init memory
	memory.Init()

	createOrUpdateHTTPTestServer()
}

// Setup overide the db credentials with test credentials
func Setup() {
	config.DatabaseURL = config.TestDatabaseURL
	config.DatabaseName = config.TestDatabaseName
}

// MainTearDown close httptest.server conn and drops all the test db collections
func MainTearDown() {
	if tServer != nil {
		tServer.Close()
	}
	TearDown()
}

func TearDown() {
	log.Println("Tear down!!!")
}
