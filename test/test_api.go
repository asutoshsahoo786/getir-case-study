package test

// TAPI test api helper struct
type TAPI struct {
	Path   string // relative path of api
	Client *Client
}

// NewDummyTAPI returns
func NewDummyTAPI(path string) *TAPI {
	return &TAPI{
		Path:   path,
		Client: GetTClient(),
	}
}
