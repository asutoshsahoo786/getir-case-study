package test

import (
	"bytes"
	"encoding/json"
	v1 "getir/api/v1"
	"io"
	"log"
	"net/http"
	"net/http/httptest"

	"github.com/go-chi/chi/v5"
)

// Test setup -----------------------------------------------------------------
var (
	tServer *httptest.Server
	tClient *Client
)

func createOrUpdateHTTPTestServer() {
	router := chi.NewRouter()
	router.Route("/v1", v1.Init)

	if tServer == nil {
		tServer = httptest.NewServer(router)
	}
	if tClient == nil {
		tClient = NewClient(tServer.URL)
	}
}

// GetTClient returns the test client connection object.
// It should called after the test.MainSetup() call.
func GetTClient() *Client {
	if tClient == nil { // TODO: update conn if needed, should not happen
		log.Fatal("client is null")
	}
	return tClient
}

// Client ...
type Client struct {
	URL        string
	Version    string
	HTTPClient *http.Client
}

// NewClient ...
func NewClient(url string) *Client {
	return &Client{
		URL:        url,
		Version:    "/v1",
		HTTPClient: &http.Client{},
	}
}

// Get makes http.get req
func (c *Client) Get(url string) (*http.Response, error) {
	return c.Do(http.MethodGet, c.URL+c.Version+url, nil)
}

// Post makes http.Post req
func (c *Client) Post(url string, body interface{}) (*http.Response, error) {
	b, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	return c.Do(http.MethodPost, c.URL+c.Version+url, bytes.NewReader(b))
}

// Do makes http request
func (c *Client) Do(method, url string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}
