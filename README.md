# Getir Case Study

App: [Heroku](https://getir-case-study-asutosh.herokuapp.com)

APIs: [Postman](https://www.postman.com/dark-firefly-422445/workspace/getir-case-study/collection/1548325-85de22f5-8130-443e-bc80-05b8366bc7fc)

## Pre-requisites to run the project

Golang should be installed on your machine.

## Running the Project Locally

```bash
go build && ./getir ./config.json
```
