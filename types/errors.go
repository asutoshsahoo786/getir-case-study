package types

import (
	"fmt"
)

func errRequired(field string) error {
	return fmt.Errorf("%s required", field)
}
