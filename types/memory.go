package types

type KV struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func (kv *KV) Ok() error {
	if ts(kv.Key) == "" {
		return errRequired("key")
	}
	if ts(kv.Value) == "" {
		return errRequired("value")
	}

	return nil
}
