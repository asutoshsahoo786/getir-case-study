package types

import (
	"fmt"
	"time"
)

type RecordFetchReq struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
	MinCount  int    `json:"minCount"`
	MaxCount  int    `json:"maxCount"`
}

const RecordFetchDateFormat = "2006-01-02"

func (r *RecordFetchReq) Ok() error {
	if ts(r.StartDate) == "" {
		return errRequired("startDate")
	}
	if ts(r.EndDate) == "" {
		return errRequired("endDate")
	}

	if _, err := time.Parse(RecordFetchDateFormat, r.StartDate); err != nil {
		return fmt.Errorf("%s is invalid, expected_format: %s, got: %s", "startDate", RecordFetchDateFormat, r.StartDate)
	}
	if _, err := time.Parse(RecordFetchDateFormat, r.EndDate); err != nil {
		return fmt.Errorf("%s is invalid, expected_format: %s, got: %s", "endDate", RecordFetchDateFormat, r.EndDate)
	}

	if r.MinCount < 0 {
		return fmt.Errorf("minCount can't be negative")
	}
	if r.MaxCount < 0 {
		return fmt.Errorf("maxCount can't be negative")
	}

	if r.MinCount > r.MaxCount {
		return fmt.Errorf("minCount can't be greater than maxCount")
	}

	return nil
}

type RecordFetchStoreReq struct {
	StartDate time.Time
	EndDate   time.Time
	MinCount  int
	MaxCount  int
}

type RecordFetchResp struct {
	Code    int           `json:"code"`
	Message string        `json:"msg"`
	Records []*RecordResp `json:"records,omitempty"`
}

type RecordResp struct {
	Key        string    `json:"key"`
	CreatedAt  time.Time `json:"createdAt"`
	TotalCount int       `json:"totalCount"`
}
