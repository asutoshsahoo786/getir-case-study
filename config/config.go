package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

var (
	// Port refers to the port at which the app server will run
	Port string
	// DatabaseURL refers to the mongo databaseURL to which the app server talks to
	DatabaseURL string
	// DatabaseName refers to the mongo database name to which the app server talks to
	DatabaseName string
)

// TEST Credentials ------------------------------------------------------------
// NOTE: Ideally, the TestDatabaseURL must be different than the actual one used in prod
// but for the sake of this assignment, I'm having it as the same
var (
	TestDatabaseURL  = "mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true"
	TestDatabaseName = "getir-case-study"
)

type runtimeValue struct {
	ptr   *string
	value string
}

var vars = map[string]runtimeValue{
	"PORT":          {&Port, "80"},
	"DATABASE_URL":  {&DatabaseURL, "mongodb://localhost:27017"},
	"DATABASE_NAME": {&DatabaseName, "getir"},
}

var cfg = map[string]string{}

var initDone = false

func loadFromJSON(files ...string) {
	for _, f := range files {
		bs, err := ioutil.ReadFile(f)
		if err != nil {
			log.Fatalf("[ERROR] unable to read config file(%s): %s\n", f, err)
		}
		if err := json.Unmarshal(bs, &cfg); err != nil {
			log.Fatalf("[ERROR] unable to unmarshall contents of config file(%s): %s\n", f, err)
		}
	}
}

func mustEnv(key string, ptr *string, defaultVal string) {
	if val, ok := cfg[key]; ok {
		*ptr = val
		log.Printf("%s value read from config file", key)
		return
	}

	if *ptr = os.Getenv(key); *ptr == "" {
		*ptr = defaultVal
		log.Printf("%s doesn't have value set in config or env, using default value", key)
		return
	}
	log.Printf("%s value read from env", key)
}

func Init(files ...string) {
	if initDone {
		panic("config init already done!")
	}

	loadFromJSON(files...)

	for key, rv := range vars {
		mustEnv(key, rv.ptr, rv.value)
	}

	initDone = true
}
