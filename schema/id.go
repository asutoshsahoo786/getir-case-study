package schema

import "go.mongodb.org/mongo-driver/bson/primitive"

type ID = primitive.ObjectID
