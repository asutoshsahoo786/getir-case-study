package schema

import "time"

type Record struct {
	ID        ID        `bson:"_id"`
	Key       string    `bson:"key"`
	Value     string    `bson:"value"`
	CreatedAt time.Time `bson:"createdAt"`
	Counts    []string  `bson:"counts"`
}
