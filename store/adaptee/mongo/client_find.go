package mongo

import "go.mongodb.org/mongo-driver/mongo/options"

// FindOneOption function is used to fetch data from the database for the passed query
// with option included
func (c *Client) FindOneOption(collection string, query interface{}, out interface{}, opts ...*options.FindOneOptions) error {
	return c.DB.Collection(collection).FindOne(bg(), query, opts...).Decode(out)
}

// FindOne function is used to fetch data from the database for the passed query
func (c *Client) FindOne(collection string, query interface{}, out interface{}) error {
	return c.FindOneOption(collection, query, out)
}

// FindAllOption function is used to fetch data from the database for the passed query with options included
// and store it in an array of elements
func (c *Client) FindAllOption(collection string, query interface{}, out interface{}, opts ...*options.FindOptions) error {
	i, err := c.DB.Collection(collection).Find(bg(), query, opts...)
	if err != nil {
		return err
	}
	defer i.Close(bg())
	return i.All(bg(), out)
}

// FindAll function is used to fetch data from the database for the passed query
// and store it in an array of elements
func (c *Client) FindAll(collection string, query interface{}, out interface{}) error {
	return c.FindAllOption(collection, query, out)
}
