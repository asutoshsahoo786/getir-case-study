package mongo

import (
	"context"
	"getir/config"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	bg = context.Background
)

func Init() *Client {
	client, err := mongo.Connect(bg(), options.Client().
		ApplyURI(config.DatabaseURL),
	)
	if err != nil {
		log.Fatalf("%+v\n", err)
	}

	db := client.Database(config.DatabaseName)
	return NewClient(db)
}
