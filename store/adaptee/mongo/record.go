package mongo

import (
	"getir/errors"
	"getir/types"
)

type Record struct {
	*Client
}

func NewRecordStore(c *Client) *Record {
	return &Record{c}
}

func (r *Record) collName() string {
	return "records"
}

func (r *Record) Fetch(req *types.RecordFetchStoreReq) ([]*types.RecordResp, *errors.AppError) {

	records := make([]*types.RecordResp, 0)
	pipeline := []types.JSON{
		{
			"$project": types.JSON{
				"_id":        0,
				"key":        1,
				"createdAt":  1,
				"totalCount": types.JSON{"$sum": "$counts"},
			},
		},
		{
			"$match": types.JSON{
				"createdAt": types.JSON{
					"$gte": req.StartDate,
					"$lte": req.EndDate,
				},
				"totalCount": types.JSON{
					"$gte": req.MinCount,
					"$lte": req.MaxCount,
				},
			},
		},
	}

	if err := r.Aggregate(r.collName(), pipeline, &records); err != nil {
		if errors.IsMongoNoDocErr(err) {
			return records, nil
		}
		return records, errors.InternalServerStd().AddDebug(err)
	}

	return records, nil
}
