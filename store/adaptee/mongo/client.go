package mongo

import (
	"getir/store/adapter"

	"go.mongodb.org/mongo-driver/mongo"
)

type Client struct {
	DB         *mongo.Database
	RecordConn adapter.RecordStore
}

func NewClient(db *mongo.Database) *Client {
	c := &Client{
		DB: db,
	}

	c.RecordConn = NewRecordStore(c)

	return c
}

func (c *Client) Records() adapter.RecordStore {
	return c.RecordConn
}
