package mongo

// Aggregate is used to run aggregate queries and decode the output into a list
func (c *Client) Aggregate(collection string, query interface{}, out interface{}) error {
	i, err := c.DB.Collection(collection).Aggregate(bg(), query)
	if err != nil {
		return err
	}
	defer i.Close(bg())
	return i.All(bg(), out)
}
