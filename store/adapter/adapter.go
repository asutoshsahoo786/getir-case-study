package adapter

import (
	"getir/errors"
	"getir/types"
)

type Store interface {
	Records() RecordStore
}

type RecordStore interface {
	Fetch(req *types.RecordFetchStoreReq) ([]*types.RecordResp, *errors.AppError)
}
