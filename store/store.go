package store

import (
	"getir/store/adaptee/mongo"
	"getir/store/adapter"
)

var Store adapter.Store

func Init() {
	Store = mongo.Init()
}
