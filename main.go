package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	v1 "getir/api/v1"
	"getir/config"
	"getir/memory"
	"getir/store"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {
	config.Init(os.Args[1:]...)
	store.Init()
	memory.Init()

	r := chi.NewRouter()

	r.Use(
		middleware.RealIP,
		middleware.Logger,
		middleware.Recoverer,
	)

	r.Route("/v1", v1.Init)

	log.Println("Starting server on port:", config.Port)
	if err := http.ListenAndServe(fmt.Sprintf(":%s", config.Port), r); err != nil {
		panic(err)
	}
}
