package respond

import (
	"encoding/json"
	"log"
	"net/http"
)

// response contains parameters that define a http response
type response struct {
	statusCode int
	headers    http.Header
	body       interface{}
}

func (r *response) Send(w http.ResponseWriter) {
	w.Header().Add("Content-Type", "application/json")
	if r.headers != nil {
		for key, values := range r.headers {
			for _, value := range values {
				w.Header().Add(key, value)
			}
		}
	}

	w.WriteHeader(r.statusCode)

	if r.statusCode != http.StatusNoContent {
		if err := json.NewEncoder(w).Encode(r.body); err != nil {
			log.Printf("[ERROR] response.send.error: %s\n", err)
		}
	}
}

func NewResponse(statusCode int, headers http.Header, body interface{}) *response {
	return &response{statusCode, headers, body}
}

func SendResponse(w http.ResponseWriter, statusCode int, headers http.Header, body interface{}) {
	NewResponse(statusCode, headers, body).Send(w)
}
