package respond

import (
	"getir/errors"
	"net/http"
)

func OK(w http.ResponseWriter, body interface{}) {
	SendResponse(w, http.StatusOK, nil, body)
}

func Created(w http.ResponseWriter, body interface{}) {
	SendResponse(w, http.StatusCreated, nil, body)
}

func Fail(w http.ResponseWriter, e *errors.AppError) {
	var body interface{}
	body = e
	if e.Payload != nil {
		body = e.Payload
	}
	SendResponse(w, e.StatusCode, nil, body)
}
