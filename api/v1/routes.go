package v1

import (
	"getir/api/v1/memory"
	"getir/api/v1/records"

	"github.com/go-chi/chi/v5"
)

func Init(r chi.Router) {
	r.Route("/records", records.Init)
	r.Route("/memory", memory.Init)
}
