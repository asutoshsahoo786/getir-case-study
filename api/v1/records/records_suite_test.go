package records_test

import (
	"fmt"
	"getir/test"
	"getir/types"
	"net/http"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestRecords(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Records Suite")
}

var _ = BeforeSuite(func() {
	// connect to database
	test.MainSetup()
	newTestAPI()
})

var _ = AfterSuite(func() {
	test.MainTearDown()
})

// -------------------------------------------------------------------
var tAPI *tRecord

// Organization/postmortem-templates endpoint helpers
type tRecord struct {
	*test.TAPI
}

func newTestAPI() {
	tAPI = &tRecord{
		TAPI: test.NewDummyTAPI("/records"),
	}
}

// POST {{getir_host}}/v1/records
func (t *tRecord) fetch(body types.RecordFetchReq) (*http.Response, error) {
	return t.Client.Post(fmt.Sprintf(t.Path), body)
}
