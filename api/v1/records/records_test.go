package records_test

import (
	"encoding/json"
	"fmt"
	"getir/types"
	"net/http"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Records", func() {
	Context("bad request", func() {
		When("startDate is empty", func() {
			var (
				res       *http.Response
				err       error
				recordReq types.RecordFetchReq
				response  types.RecordFetchResp
			)

			JustBeforeEach(func() {
				recordReq = types.RecordFetchReq{
					StartDate: "",
					EndDate:   "",
					MinCount:  -1,
					MaxCount:  -1,
				}
				res, err = tAPI.fetch(recordReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Code).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal("startDate required"))
				Expect(len(response.Records)).To(Equal(0))
			})
		})

		When("endDate is empty", func() {
			var (
				res       *http.Response
				err       error
				recordReq types.RecordFetchReq
				response  types.RecordFetchResp
			)

			JustBeforeEach(func() {
				recordReq = types.RecordFetchReq{
					StartDate: "26th Jan, 2016",
					EndDate:   "",
					MinCount:  -1,
					MaxCount:  -1,
				}
				res, err = tAPI.fetch(recordReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Code).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal("endDate required"))
				Expect(len(response.Records)).To(Equal(0))
			})
		})
		When("startDate has invalid format", func() {
			var (
				res       *http.Response
				err       error
				recordReq types.RecordFetchReq
				response  types.RecordFetchResp
			)

			JustBeforeEach(func() {
				recordReq = types.RecordFetchReq{
					StartDate: "26th Jan, 2016",
					EndDate:   "2nd Feb, 2018",
					MinCount:  -1,
					MaxCount:  -1,
				}
				res, err = tAPI.fetch(recordReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Code).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal(
					fmt.Sprintf(
						"startDate is invalid, expected_format: %s, got: %s", types.RecordFetchDateFormat, recordReq.StartDate,
					),
				))
				Expect(len(response.Records)).To(Equal(0))
			})
		})
		When("endDate has invalid format", func() {
			var (
				res       *http.Response
				err       error
				recordReq types.RecordFetchReq
				response  types.RecordFetchResp
			)

			JustBeforeEach(func() {
				recordReq = types.RecordFetchReq{
					StartDate: "2016-01-26",
					EndDate:   "2nd Feb, 2018",
					MinCount:  -1,
					MaxCount:  -1,
				}
				res, err = tAPI.fetch(recordReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Code).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal(
					fmt.Sprintf(
						"endDate is invalid, expected_format: %s, got: %s", types.RecordFetchDateFormat, recordReq.EndDate,
					),
				))
				Expect(len(response.Records)).To(Equal(0))
			})
		})
		When("minCount is negative", func() {
			var (
				res       *http.Response
				err       error
				recordReq types.RecordFetchReq
				response  types.RecordFetchResp
			)

			JustBeforeEach(func() {
				recordReq = types.RecordFetchReq{
					StartDate: "2016-01-26",
					EndDate:   "2018-02-02",
					MinCount:  -1,
					MaxCount:  -1,
				}
				res, err = tAPI.fetch(recordReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Code).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal("minCount can't be negative"))
				Expect(len(response.Records)).To(Equal(0))
			})
		})
		When("maxCount is negative", func() {
			var (
				res       *http.Response
				err       error
				recordReq types.RecordFetchReq
				response  types.RecordFetchResp
			)

			JustBeforeEach(func() {
				recordReq = types.RecordFetchReq{
					StartDate: "2016-01-26",
					EndDate:   "2018-02-02",
					MinCount:  2700,
					MaxCount:  -1,
				}
				res, err = tAPI.fetch(recordReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Code).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal("maxCount can't be negative"))
				Expect(len(response.Records)).To(Equal(0))
			})
		})
		When("minCount is greater than maxCount", func() {
			var (
				res       *http.Response
				err       error
				recordReq types.RecordFetchReq
				response  types.RecordFetchResp
			)

			JustBeforeEach(func() {
				recordReq = types.RecordFetchReq{
					StartDate: "2016-01-26",
					EndDate:   "2018-02-02",
					MinCount:  2900,
					MaxCount:  2700,
				}
				res, err = tAPI.fetch(recordReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Code).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal("minCount can't be greater than maxCount"))
				Expect(len(response.Records)).To(Equal(0))
			})
		})
	})
	Context("success", func() {
		When("input is as expected", func() {
			var (
				res       *http.Response
				err       error
				recordReq types.RecordFetchReq
				response  types.RecordFetchResp
				startDate time.Time
				endDate   time.Time
			)

			JustBeforeEach(func() {
				recordReq = types.RecordFetchReq{
					StartDate: "2016-01-26",
					EndDate:   "2018-02-02",
					MinCount:  2700,
					MaxCount:  2900,
				}
				res, err = tAPI.fetch(recordReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusOK))
				Expect(response.Code).To(Equal(0))
				Expect(response.Message).To(Equal("Success"))

				startDate, err = time.Parse(types.RecordFetchDateFormat, recordReq.StartDate)
				Expect(err).ShouldNot(HaveOccurred())
				endDate, err = time.Parse(types.RecordFetchDateFormat, recordReq.EndDate)
				Expect(err).ShouldNot(HaveOccurred())

				for _, record := range response.Records {
					Expect(record.CreatedAt.After(startDate)).Should(BeTrue())
					Expect(record.CreatedAt.Before(endDate)).Should(BeTrue())
					Expect(record.TotalCount >= recordReq.MinCount).Should(BeTrue())
					Expect(record.TotalCount <= recordReq.MaxCount).Should(BeTrue())
				}
			})
		})
	})
})
