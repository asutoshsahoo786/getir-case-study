package records

import (
	"getir/errors"
	"getir/respond"
	"getir/store"
	"getir/types"
	"getir/utils"
	"net/http"
	"time"
)

func recordsHandler(w http.ResponseWriter, r *http.Request) *errors.AppError {
	var req types.RecordFetchReq
	if err := utils.Decode(r.Body, &req); err != nil {
		return attachPayload(errors.BadRequest(err.Error()))
	}

	startDate, _ := time.Parse(types.RecordFetchDateFormat, req.StartDate)
	endDate, _ := time.Parse(types.RecordFetchDateFormat, req.EndDate)

	records, err := store.Store.Records().Fetch(&types.RecordFetchStoreReq{
		StartDate: startDate,
		EndDate:   endDate,
		MinCount:  req.MinCount,
		MaxCount:  req.MaxCount,
	})
	if err != nil {
		return attachPayload(err)
	}

	respond.OK(w, types.RecordFetchResp{
		Code:    0,
		Message: "Success",
		Records: records,
	})

	return nil
}

func attachPayload(err *errors.AppError) *errors.AppError {
	return err.AddPayload(
		types.RecordFetchResp{
			Code:    err.StatusCode,
			Message: err.Error(),
		},
	)
}
