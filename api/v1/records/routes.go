package records

import (
	"getir/api"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func Init(r chi.Router) {
	r.Method(http.MethodPost, "/", api.Handler(recordsHandler))
}
