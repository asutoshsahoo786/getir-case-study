package memory

import (
	"getir/errors"
	"getir/memory"
	"getir/respond"
	"getir/types"
	"getir/utils"
	"net/http"
)

func setKVHandler(w http.ResponseWriter, r *http.Request) *errors.AppError {
	var req types.KV
	if err := utils.Decode(r.Body, &req); err != nil {
		return errors.BadRequest(err.Error())
	}

	memory.Store(req.Key, req.Value)

	respond.OK(w, req)
	return nil
}
