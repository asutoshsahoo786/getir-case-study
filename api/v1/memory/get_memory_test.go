package memory_test

import (
	"encoding/json"
	"getir/errors"
	"getir/memory"
	"getir/types"
	"net/http"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("GetMemory", func() {
	Context("bad request", func() {
		When("empty key queried", func() {
			var (
				res      *http.Response
				err      error
				response *errors.AppError
			)

			JustBeforeEach(func() {
				res, err = tAPI.getKV("")
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal("key required"))
			})
		})
	})

	Context("not found", func() {
		When("key not set", func() {
			var (
				res      *http.Response
				err      error
				response *errors.AppError
			)

			JustBeforeEach(func() {
				res, err = tAPI.getKV("something")
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as not found", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusNotFound))
				Expect(response.StatusCode).To(Equal(http.StatusNotFound))
				Expect(response.Message).To(Equal("entry not found against key: 'something'"))
			})
		})
	})

	Context("success", func() {
		When("key found", func() {
			var (
				res      *http.Response
				err      error
				kv       types.KV
				response types.KV
			)

			BeforeEach(func() {
				kv = types.KV{
					Key:   "something",
					Value: "great",
				}
				memory.Store(kv.Key, kv.Value)
			})
			JustBeforeEach(func() {
				res, err = tAPI.getKV(kv.Key)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond back the KV pair", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusOK))
				Expect(response.Key).To(Equal(kv.Key))
				Expect(response.Value).To(Equal(kv.Value))
			})
			AfterEach(func() {
				memory.Delete(kv.Key)
			})
		})
	})
})
