package memory

import (
	"fmt"
	"getir/errors"
	"getir/memory"
	"getir/respond"
	"getir/types"
	"net/http"
	"strings"
)

func getKVHandler(w http.ResponseWriter, r *http.Request) *errors.AppError {
	key := strings.TrimSpace(r.URL.Query().Get("key"))
	if key == "" {
		return errors.BadRequest("key required")
	}

	val, ok := memory.Load(key)
	if !ok {
		return errors.NotFound(fmt.Sprintf("entry not found against key: '%s'", key))
	}

	respond.OK(w, types.KV{
		Key:   key,
		Value: val,
	})
	return nil
}
