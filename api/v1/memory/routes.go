package memory

import (
	"getir/api"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func Init(r chi.Router) {
	r.Method(http.MethodGet, "/", api.Handler(getKVHandler))
	r.Method(http.MethodPost, "/", api.Handler(setKVHandler))
}
