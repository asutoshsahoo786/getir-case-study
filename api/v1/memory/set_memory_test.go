package memory_test

import (
	"encoding/json"
	"getir/errors"
	"getir/memory"
	"getir/types"
	"net/http"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("SetMemory", func() {
	Context("bad request", func() {
		When("key is empty", func() {
			var (
				res      *http.Response
				err      error
				response *errors.AppError
			)

			JustBeforeEach(func() {
				res, err = tAPI.setKV(types.KV{
					Key:   "",
					Value: "",
				})
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal("key required"))
			})
		})

		When("value is empty", func() {
			var (
				res      *http.Response
				err      error
				response *errors.AppError
			)

			JustBeforeEach(func() {
				res, err = tAPI.setKV(types.KV{
					Key:   "something",
					Value: "",
				})
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should respond as bad request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
				Expect(response.Message).To(Equal("value required"))
			})
		})
	})

	Context("success", func() {
		When("input is as expected", func() {
			var (
				res      *http.Response
				err      error
				kvReq    types.KV
				response types.KV
			)
			JustBeforeEach(func() {
				kvReq = types.KV{
					Key:   "something",
					Value: "great",
				}
				res, err = tAPI.setKV(kvReq)
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should echo the request", func() {
				err = json.NewDecoder(res.Body).Decode(&response)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res.StatusCode).To(Equal(http.StatusOK))
				Expect(response.Key).To(Equal(kvReq.Key))
				Expect(response.Value).To(Equal(kvReq.Value))
			})
			AfterEach(func() {
				memory.Delete(kvReq.Key)
			})
		})
	})

})
