package memory_test

import (
	"fmt"
	"getir/test"
	"getir/types"
	"net/http"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestMemory(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Memory Suite")
}

var _ = BeforeSuite(func() {
	// connect to database
	test.MainSetup()
	newTestAPI()
})

var _ = AfterSuite(func() {
	test.MainTearDown()
})

// -------------------------------------------------------------------
var tAPI *tMemory

// Organization/postmortem-templates endpoint helpers
type tMemory struct {
	*test.TAPI
}

func newTestAPI() {
	tAPI = &tMemory{
		TAPI: test.NewDummyTAPI("/memory"),
	}
}

// GET {{getir_host}}/v1/memory?key=<key>
func (t *tMemory) getKV(key string) (*http.Response, error) {
	return t.Client.Get(fmt.Sprintf("%s?key=%s", t.Path, key))
}

// POST {{getir_host}}/v1/memory
func (t *tMemory) setKV(body types.KV) (*http.Response, error) {
	return t.Client.Post(fmt.Sprintf(t.Path), body)
}
