package api

import (
	"getir/errors"
	"getir/respond"
	"log"
	"net/http"
)

type Handler func(w http.ResponseWriter, r *http.Request) *errors.AppError

func (f Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	e := f(w, r)
	if e != nil {
		log.Printf(
			"[ERROR] StatusCode: %d, Message: %s, Debug: %s, Payload: %v\n",
			e.StatusCode, e.Error(), e.Debug, e.Payload,
		)
		respond.Fail(w, e)
	}
}
