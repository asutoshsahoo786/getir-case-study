package errors

import "go.mongodb.org/mongo-driver/mongo"

// IsMongoNoDocErr should return true if the err is redis: nil
func IsMongoNoDocErr(err error) bool {
	return err == mongo.ErrNoDocuments
}
