package errors

import (
	"fmt"
	"net/http"
)

type AppError struct {
	StatusCode int         `json:"status"`
	Message    string      `json:"message"`
	Debug      error       `json:"-"`
	Payload    interface{} `json:"-"`
}

func (e *AppError) Error() string {
	return e.Message
}

func (e *AppError) AddDebug(err error) *AppError {
	e.Debug = err
	return e
}

func (e *AppError) AddDebugf(format string, a ...interface{}) *AppError {
	return e.AddDebug(fmt.Errorf(format, a...))
}

func (e *AppError) AddPayload(payload interface{}) *AppError {
	e.Payload = payload
	return e
}

func NewAppError(statusCode int, message string) *AppError {
	return &AppError{StatusCode: statusCode, Message: message}
}

func BadRequest(message string) *AppError {
	return NewAppError(http.StatusBadRequest, message)
}

func NotFound(message string) *AppError {
	return NewAppError(http.StatusNotFound, message)
}

func InternalServer(message string) *AppError {
	return NewAppError(http.StatusInternalServerError, message)
}

func InternalServerStd() *AppError {
	return InternalServer("something went wrong")
}
