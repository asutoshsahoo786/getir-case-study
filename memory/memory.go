package memory

import "sync"

type memory struct {
	sync.RWMutex
	kv map[string]string
}

func (m *memory) load(key string) (string, bool) {
	m.RLock()
	defer m.RUnlock()
	val, ok := m.kv[key]
	return val, ok
}

func (m *memory) store(key, val string) {
	m.Lock()
	defer m.Unlock()
	m.kv[key] = val
}

func (m *memory) delete(key string) {
	m.Lock()
	defer m.Unlock()
	delete(m.kv, key)
}

func new() *memory {
	return &memory{
		kv: make(map[string]string),
	}
}

var memStore *memory

func Init() {
	memStore = new()
}

func Load(key string) (string, bool) {
	return memStore.load(key)
}

func Store(key, val string) {
	memStore.store(key, val)
}

func Delete(key string) {
	memStore.delete(key)
}
